# Sales Taxes

## Prerequisites

1. PHP installed
    
## Installation

1. Download from repository

        https://bitbucket.org/AlessioCoser/sales-taxes-problem/downloads

2. Extract into a folder

        unzip /to/my/folder/

3. Enter into project folder

        cd /to/my/folder/AlessioCoser-sales-taxes-problem-692193d13664
    
## Examples

If you are in the project folder you can run the examples with the commands below

    php examples/output1.php
    php examples/output2.php
    php examples/output3.php
    
Each example will print the receipt on the command line

## Usage

First of all you have to include the autoload file
    
        include_once( "../src/autoload.php");
        
Create a product

        // $prod = new Product( name:string, type:string, price:float, [is_imported:bool = false], [quantity:int = 1])
        
        $product = new Product( "name", "type", 3.43, true )
        $product2 = new Product( "my great book", "books", 12.90 );
        $product3 = new Product( "CD greatest hits", "music", 5.30, true, 2 );
        
        
In order to create a receipt you have to create at least one product

        $receipt = new Receipt( [ $product, $product2 ] );
        
You can add a product before object Receipt creation 

        $receipt->addProduct( $product3 );
        $receipt->addProduct( new Product( "beautiful product", "other", 99.99 ) );
        
You can set all tax and duty settings

        $receipt->setSalesTax(10);   // 10% basic sales tax
        $receipt->setImportDuty(5);  // 5% import duty
        // types of products that are exempt from basic sales tax
        $receipt->setSalesExemption( [ "books", "food", "medical" ] );

When all is configured you can generate taxes
        
        $receipt->generateTaxes();
        
Then you can get the amount of taxes, the total price or the whole receipt
        
        $totalPrice = $receipt->getTotalPrice();
        $totalTaxes = $receipt->getTotalTaxes();
        $textReceipt = $receipt->printReceipt();
        

## Testing

In order to run the test, you have to install Composer which allows you to manage the dependencies
    
    curl -sS https://getcomposer.org/installer | php
            
Now you can use Composer to install the dependencies (PHPUnit)
        
    php composer.phar install
        
ReceiptTest.php checks the outputs with the 3 types of input

    Input 1:
    1 book at 12.49
    1 music CD at 14.99
    1 chocolate bar at 0.85
    
    Input 2:
    1 imported box of chocolates at 10.00
    1 imported bottle of perfume at 47.50
    
    Input 3:
    1 imported bottle of perfume at 27.99
    1 bottle of perfume at 18.99
    1 packet of headache pills at 9.75
    1 box of imported chocolates at 11.25


Use this command to execute the test class: ReceiptTest.php

    vendor/bin/phpunit tests/ReceiptTest.php

In ReceiptTest.php all the generated amounts are compared with the following amounts
 
    Output 1:
    Total: 29.83
    
    Output 2:
    Total: 65.15
    
    Output 3:
    Total: 74.68
    
If the test is passed, the result is something like the code below:

    
    ...                                             3 / 3 (100%)
    
    Time: 44 ms, Memory: 2.50Mb
    
    OK (3 tests, 3 assertions)




