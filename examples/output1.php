<?php
include_once( __DIR__ . "/../src/autoload.php");

$receipt = new Receipt( [
    new Product( "book", "books", 12.49),
    new Product( "music CD", "music", 14.99),
    new Product( "chocolate bar", "food", 0.85)
]);
$receipt->setSalesTax(10);
$receipt->setImportDuty(5);
$receipt->setSalesExemption( [ "books", "food", "medical" ] );
$receipt->generateTaxes();

echo PHP_EOL . "Output 1:" . PHP_EOL;
echo $receipt->printReceipt() . PHP_EOL . PHP_EOL;