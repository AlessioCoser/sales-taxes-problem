<?php
include_once( __DIR__ . "/../src/autoload.php");

$receipt = new Receipt( [
    new Product( "imported box of chocolates", "food", 10.00, true),
    new Product( "imported bottle of perfume", "beauty", 47.50, true)
]);
$receipt->setSalesTax(10);
$receipt->setImportDuty(5);
$receipt->setSalesExemption( [ "books", "food", "medical" ] );
$receipt->generateTaxes();

echo PHP_EOL . "Output 2:" . PHP_EOL;
echo $receipt->printReceipt() . PHP_EOL . PHP_EOL;