<?php
include_once( __DIR__ . "/../src/autoload.php");

$receipt = new Receipt( [
    new Product( "imported bottle of perfume", "beauty", 27.99, true),
    new Product( "bottle of perfume", "beauty", 18.99),
    new Product( "packet of headache pills", "medical", 9.75),
    new Product( "imported box of chocolates", "food", 11.25, true)
]);
$receipt->setSalesTax(10);
$receipt->setImportDuty(5);
$receipt->setSalesExemption( [ "books", "food", "medical" ] );
$receipt->generateTaxes();

echo PHP_EOL . "Output 3:" . PHP_EOL;
echo $receipt->printReceipt() . PHP_EOL . PHP_EOL;