<?php
include_once( __DIR__ . "/../src/autoload.php");

class ReceiptTest extends PHPUnit_Framework_TestCase
{
    public function testOutput1()
    {
        $receipt = new Receipt( [
            new Product( "book", "books", 12.49),
            new Product( "music CD", "music", 14.99),
            new Product( "chocolate bar", "food", 0.85)
        ]);
        $receipt->setSalesTax(10);
        $receipt->setImportDuty(5);
        $receipt->setSalesExemption( [ "books", "food", "medical" ] );
        $receipt->generateTaxes();

        $this->assertEquals("29.83", (string)$receipt->getTotalPrice());
    }

    public function testOutput2()
    {
        $receipt = new Receipt( [
            new Product( "imported box of chocolates", "food", 10.00, true),
            new Product( "imported bottle of perfume", "beauty", 47.50, true)
        ]);
        $receipt->setSalesTax(10);
        $receipt->setImportDuty(5);
        $receipt->setSalesExemption( [ "books", "food", "medical" ] );
        $receipt->generateTaxes();

        $this->assertEquals("65.15", (string)$receipt->getTotalPrice());
    }

    public function testOutput3()
    {
        $receipt = new Receipt( [
            new Product( "imported bottle of perfume", "beauty", 27.99, true),
            new Product( "bottle of perfume", "beauty", 18.99),
            new Product( "packet of headache pills", "medical", 9.75),
            new Product( "imported box of chocolates", "food", 11.25, true)
        ]);
        $receipt->setSalesTax(10);
        $receipt->setImportDuty(5);
        $receipt->setSalesExemption( [ "books", "food", "medical" ] );
        $receipt->generateTaxes();

        $this->assertEquals("74.68", (string)$receipt->getTotalPrice());
    }
}

