<?php
/*!
    Copyright (C) 2015  Alessio Coser

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Product
{
    protected $name;
    protected $type;
    protected $price = 0;
    protected $tax_amount = 0.00;
    protected $is_imported = false;
    protected $quantity;

    /**
     * @param string $name
     * @param string $type
     * @param float $price
     * @param bool $isImported
     * @param int $quantity
     */
    public function __construct( $name, $type, $price, $isImported = false, $quantity = 1 )
    {
        $this->name = $name;
        $this->type = $type;
        $this->price = floatval($price);
        $this->is_imported = $isImported;
        $this->quantity = intval($quantity);
    }

    /**
     * @return bool
     */
    public function isImported(){
        return $this->is_imported;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @return float
     */
    public function getPrice(){
        return $this->price;
    }

    /**
     * @return int
     */
    public function getQuantity(){
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getSingleTaxAmount(){
        return $this->tax_amount;
    }

    /**
     * @return float
     */
    public function getTaxAmount(){
        return $this->tax_amount * $this->quantity;
    }

    /**
     * @return float
     */
    public function getFullSinglePrice(){
        return $this->price + $this->tax_amount;
    }

    /**
     * @return float
     */
    public function getFullPrice(){
        return ( $this->price + $this->tax_amount ) * $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity( $quantity ){
        $this->quantity = intval($quantity);
    }

    /**
     * @param float $taxAmount
     */
    public function setTaxAmount( $taxAmount ){
        $this->tax_amount = floatval($taxAmount);
    }

    /**
     * @param float $taxAmount
     */
    public function addTaxAmount( $taxAmount ){
        $this->tax_amount = $this->tax_amount + floatval($taxAmount);
    }

}