<?php

if(!function_exists('classAutoLoader')){
    function classAutoLoader($class){
        $classFile= __DIR__ . "/" .$class.'.php';
        if(is_file($classFile)&&!class_exists($class)) include_once $classFile;
    }
}
spl_autoload_register('classAutoLoader');