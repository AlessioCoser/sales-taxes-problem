<?php
/*!
    Copyright (C) 2015  Alessio Coser

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

class Receipt
{
    private $sales_tax = 10;
    private $sales_exemption = [];
    private $import_duty = 5;

    private $rounding_unity_duty = 0.05;

    private $total_taxes = 0.00;
    private $total_price = 0.00;

    /** @var  Product[] */
    private $products = [];

    /**
     * @param int $salesTax
     * @param array $salesExemption
     * @param int $importDuty
     */
    public function __construct( $products )
    {
        $this->products = $products;
    }

    /**
     * get sum of all prices and taxes
     * @return float
     */
    public function getTotalPrice(){
        return $this->total_price;
    }

    /**
     * get sum of all taxes
     * @return float
     */
    public function getTotalTaxes(){
        return $this->total_taxes;
    }

    /**
     * check if a product is exempted
     * @param Product $product
     * @return bool
     */
    private function isExempted( $product )
    {
        return in_array($product->getType(), $this->sales_exemption);
    }

    /**
     * get tax precentage rounded to nearest 0.05
     * @param float $value
     * @param float $percentage
     * @return float
     */
    private function taxPercentage( $value, $percentage ){
        $perc = (($value / 100 ) * $percentage );
        //round up to nearest $rounding_unity_duty value
        return ceil( $perc / $this->rounding_unity_duty ) * $this->rounding_unity_duty;
    }

    /**
     * @param int $salesTax
     */
    public function setSalesTax( $salesTax ){
        $this->sales_tax = intval($salesTax);
    }

    /**
     * @param int $importDuty
     */
    public function setImportDuty( $importDuty ){
        $this->import_duty = intval($importDuty);
    }

    /**
     * @param float $roundingUnityDuty
     */
    public function setRoundingUnityDuty( $roundingUnityDuty ){
        $this->rounding_unity_duty = floatval($roundingUnityDuty);
    }

    /**
     * @param int $importDuty
     */
    public function setSalesExemption( $salesExemption ){
        $this->sales_exemption = $salesExemption;
    }

    /**
     * @param Product $product
     */
    public function addProduct( $product ){
        $this->products[] = $product;
    }

    /**
     * set taxes by imports and exemptions
     * @return void
     */
    public function generateTaxes(){
        $this->total_taxes = 0.00;
        $this->total_price = 0.00;
        foreach( $this->products as $product )
        {
            if( !$this->isExempted( $product ) )
                $product->setTaxAmount( $this->taxPercentage( $product->getPrice(), $this->sales_tax ) );

            if( $product->isImported() )
                $product->addTaxAmount( $this->taxPercentage( $product->getPrice(), $this->import_duty ) );

            $this->total_taxes += $product->getTaxAmount();
            $this->total_price += $product->getFullPrice();
        }
    }

    /**
     * return a simple receipt
     * @return string
     */
    public function printReceipt(){
        $receipt = "";
        foreach( $this->products as $product )
        {
            $receipt .= $product->getQuantity() . " " . $product->getName() . ": " . number_format($product->getFullPrice(), 2) . PHP_EOL;
        }
        $receipt .= "Sales Taxes: " . number_format($this->total_taxes, 2) . PHP_EOL;
        $receipt .= "Total: " . number_format($this->total_price, 2);

        return $receipt;
    }


}